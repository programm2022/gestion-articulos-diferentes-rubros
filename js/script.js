fetch("rubros.csv")
    .then(function (res) {

        return (res.text());

    })

    .then(function (data) {

        cargaRubros(data);
    })


fetch("lista.csv")
    .then(function (res) {        //en res nos regresa el contenido
        console.log(res);
        return (res.text());   //retorno el contenido en forma de texto
    })
    .then(function (data) { //con el contenido recibido  armamos una funcion que lo aplico
        console.log(data);
        mostrarTabla(data);
    });

    const rubros =[];
    function cargaRubros(data_rubros) {
        let filas_rubros= data_rubros.split(/\r?\n|\r/);
        for (let i = 0; i < filas_rubros.length; i++) {
            let datos_rubros = filas_rubros[i].split(",");
           /* console.log(datos_rubros);*/
            rubros[i] = {codigo: datos_rubros[0], descripcion: datos_rubros[1]}         
        }
    }

    /*console.log(rubros)
  /*  console.log(rubros[0].codigo, " - ",rubros [0]. descripcion);*/
  /*  console.log(rubros.length);*/
function mostrarTabla(contenido) {
    /*console.log(rubros[0].codigo);
    console.log(rubros.length);*/
    let tabular;
    for (let r = 1; r <= rubros.length; r++) {

        if (r === 1) {
            tabular = document.querySelector(".contenedor-computacion");
        }
        else if (r === 2) {
            tabular = document.querySelector(".contenedor-celulares");
        }
        else if (r === 3) {
            tabular = document.querySelector(".contenedor-perifericos");
        }
        else if (r === 4) {
            tabular = document.querySelector(".contenedor-electrodomesticos");
        }
        console.log("recorriendo la sección de: ", tabular);

        let filas = contenido.split(/\r?\n|\r/);

        armarTemplate(r, filas);


        tabular.innerHTML = armarTemplate(r, filas);
    }

}

function armarTemplate(r, filas) {
    let template = ``;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        if (celdasFila[0] === r.toString()) {
            console.log(celdasFila[0], " - ", celdasFila[1], " - ", celdasFila[2]);
            template += `<article>`
            template += `<img src=" ${celdasFila[1]}" alt=""/>`
            template += `<h4>${celdasFila[2]}</h4>`
            template += `</article>`
        }
    }
    return template;
}